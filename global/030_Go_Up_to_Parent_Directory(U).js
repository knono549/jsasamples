/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

(function(){
    var loc = window.location;
    var prot = loc.protocol;
    var pre = prot;
    if (loc.href.substr(prot.length, 2) == '//') { pre += '//'; }
    pre += loc.host;
    var uri;
    var srch = loc.search;
    if (srch && srch != '?') {
        uri = pre + loc.pathname;
    } else {
        var path = loc.pathname;
        var npath = path.replace(/\/[^\/]+\/?$/, '/');
        if (npath == path) {
            window.alert('Current directory is root.');
            return;
        }
        uri = pre + npath;
    }

    var flag = 0;
    if (_jsaCScript.event.shiftKey) { flag |= 1; }
    if (_jsaCScript.event.ctrlKey) { flag |= 2; }
    if (_jsaCScript.event.button > 0) { flag |= 2; }
    switch (flag) {
      case 0: _jsaCScript.tabs.update({url: uri}); break;
      case 1: _jsaCScript.windows.create({url: uri}); break;
      case 2:
        _jsaCScript.tabs.create(
            {url: uri, active: false, jsaposition: "afterCurrent"});
        break;
      case 3:
        _jsaCScript.tabs.create(
            {url: uri, jsaposition: "afterCurrent"});
        break;
    }
})();
