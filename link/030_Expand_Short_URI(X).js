/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

(function(){
    var context = _jsaCScript.context;
    if (! context.onLink) { return; }

    var uri = context.link.href;
    if (uri.length > 36 &&
        ! window.confirm("It doesn't seem to be short link.\n" +
                           "Are you sure you want to continue?")) {
        return;
    }
    _jsaCScript.tabs.create(
        {
          url: 'http://www.getlinkinfo.com/info?link=' +
            encodeURIComponent(uri),
          active: false,
          jsaposition: "afterCurrent"
        });
})();
