/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Display the image and zoom in/out rotate right/left it.
 */
function ImgBox() {
    /** root element */
    this.box = null;
    /** place holder element */
    this.ph = null;
    /** image element */
    this.img = null;
    /** angle (degree) */
    this.angle = 4242;
    /** scale factor */
    this.scaleX = NaN;
    /** scale factor */
    this.scaleY = NaN;
}
ImgBox.prototype = {
  /** ID of root element */
  id: 'JSA-IMG-BOX',
  /** CSS text */
  css:
    '#JSA-IMG-BOX {' +
    '  margin: 0 !important;' +
    '  padding: 0.25em !important;' +
    '  position: absolute !important;' +
    '  border: 1px solid #424242 !important;' +
    '  background-color: #212121 !important;' +
    '  font: medium sans-serif !important;' +
    '  text-align: center !important;' +
    '}' +
    '#JSA-IMG-BOX > div:last-of-type {' +
    '  margin: 1.5em !important;' +
    '  padding: 0 !important;' +
    '  overflow: hidden !important;' +
    '}' +
    '#JSA-IMG-BOX > div:last-of-type > img {' +
    '  position: static !important;' +
    '  -moz-transform: none !important;' +
    '  -moz-transform-origin: 50% 50% 0 !important;' +
    '}',
  /** scale factors */
  SCALES: [0.01, 0.015, 0.02, 0.03, 0.04, 0.05, 0.0625, 0.0833, 0.125, 0.167,
            0.25, 0.33, 0.5, 0.667, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0,
            7.0, 8.0, 12.0, 16.0, 32.0],
  /**
   * @return void
   */
  init: function () {
      var box = document.getElementById(this.id);
      if (box) {
          this.box = box;
          this.ph = this.box.lastChild;
          this.img = this.ph.firstChild;
          this.parseImgTransform();
          return;
      }

      var df = document.createDocumentFragment();
      /*
       * div#JSA-IMG-BOX
       *   style
       *     #text
       *   div
       */
      box = document.createElement('div');
      box.id = this.id;
      df.appendChild(box);
      var style = document.createElement('style');
      style.setAttribute('type', 'text/css');
      box.appendChild(style);
      style.appendChild(document.createTextNode(this.css));
      var ph = document.createElement('div');
      box.appendChild(ph);

      var body = document.body;
      if (! body) {
          var html = document.documentElement;
          body = document.createElement('body');
          html.insertBefore(body, html.firstChild);
      }
      body.appendChild(df);

      this.box = box;
      this.ph = ph;
  },
  /**
   * @return void
   */
  center: function () {
      var box = this.box;
      if (! box) { return; }

      var bwidth = box.offsetWidth;
      var wwidth = window.innerWidth;
      var wleft = window.pageXOffset;
      if (bwidth < wwidth) {
          var left = Math.round((wwidth - bwidth) / 2);
          box.style.setProperty('left', wleft + left + 'px', 'important');
      } else {
          var bleft = 0;
          var element = box;
          do {
              bleft += element.offsetLeft;
              element = element.offsetParent;
          } while (element);
          if (bleft - wleft > 0 || bleft + bwidth < wleft) {
              box.style.setProperty('left', wleft + 'px', 'important');
          }
      }

      var bheight = box.offsetHeight;
      var wheight = window.innerHeight;
      var wtop = window.pageYOffset;
      if (bheight < wheight) {
          var top = Math.round((wheight - bheight) / 2);
          box.style.setProperty('top', wtop + top + 'px', 'important');
      } else {
          var btop = 0;
          var element = box;
          do {
              btop += element.offsetTop;
              element = element.offsetParent;
          } while (element);
          if (btop - wtop > 0 || btop + bheight < wtop) {
              box.style.setProperty('top', wtop + 'px', 'important');
          }
      }

      box.style.setProperty('z-index', 4095, 'important');
  },
  /**
   * @return void
   */
  close: function () {
      var box = this.box;
      if (! box) { return; }

      box.parentNode.removeChild(box);

      this.box = null;
      this.ph = null;
      this.img = null;
  },
  /**
   * @return void
   */
  parseImgTransform: function () {
      var img = this.img;
      if (! img) { return; }

      var tf = img.style.getPropertyValue('-moz-transform');
      if (tf == '' || tf == 'none') {
          this.angle = 0;
          this.scaleX = 1.0;
          this.scaleY = 1.0;
          return;
      }

      var angle = 4242;
      var scalex = NaN;
      var scaley = NaN;
      var pnum = "([+-]?(?:[0-9]+(?:\\.[0-9]+)?|\\.[0-9]+))";
      var pang = pnum + "(deg|grad|rad|turn)";
      var re = new RegExp(
          "^(?:" +
          "(rotate)\\(\\s*" + pang + "\\s*\\)|" +
          "(scale)\\(\\s*" + pnum + "(?:,\\s*" + pnum + ")?" + "\\s*\\)|" +
          "(scaleX)\\(\\s*" + pnum + "\\s*\\)|" +
          "(scaleY)\\(\\s*" + pnum + "\\s*\\)" +
          ")\\s*");
      var tmp = tf;
      while (tmp.length > 0) {
          var m = tmp.match(re);
          if (! m) {
              throw new Error("Unexpected value is set.\n" +
                                "-moz-transform: " + tf);
          } else if (m[1]) { //rotate
              if (angle != 4242) {
                  throw new Error("Multiple rotate function are not " +
                                    "supported.\n" +
                                    "-moz-transform: " + tf);
              }
              var num = parseFloat(m[2]);
              switch (m[3]) {
                case 'deg': angle = num; break;
                case 'grad': angle = 360 * num / 400; break;
                case 'rad': angle = 180 * num / Math.PI; break;
                case 'turn': angle = 360 * num; break;
              }
              if (! (angle >= -360 && angle <= 360)) {
                  throw new Error("Specified value is out of range.\n" +
                                    "rotate: " + m[2] + m[3]);
              }
          } else if (m[4]) { //scale
              if (! isNaN(scalex) || ! isNaN(scaley)) {
                  throw new Error("Multiple scale(XY) function are not " +
                                    "supported.\n" +
                                    "-moz-transform: " + tf);
              }
              scalex = parseFloat(m[5]);
              scaley = m[6] ? parseFloat(m[6]) : scalex;
          } else if (m[7]) { //scaleX
              if (! isNaN(scalex)) {
                  throw new Error("Multiple scale(X) function are not " +
                                    "supported.\n" +
                                    "-moz-transform: " + tf);
              }
              scalex = parseFloat(m[8]);
          } else if (m[9]) { //scaleY
              if (! isNaN(scaley)) {
                  throw new Error("Multiple scale(Y) function are not " +
                                    "supported.\n" +
                                    "-moz-transform: " + tf);
              }
              scaley = parseFloat(m[10]);
          }
          tmp = tmp.substring(m.index + m[0].length);
      }

      this.angle = angle == 4242 ? 0 : angle;
      this.scaleX = isNaN(scalex) ? 1.0 : scalex;
      this.scaleY = isNaN(scaley) ? 1.0 : scaley;
  },
  /**
   * @param aImg HTMLImageElement
   * @return void
   */
  loadImg: function (aImg) {
      var ph = this.ph;
      if (! ph) { return; }
      var oimg = this.img;

      var img = document.createElement('img');
      img.src = aImg.src;
      img.alt = aImg.alt;
      img.title = aImg.title;
      img.width = aImg.width;
      img.height = aImg.height;
      if (oimg) {
          ph.replaceChild(img, oimg);
      } else {
          ph.appendChild(img);
      }
      ph.style.setProperty(
          'width', aImg.width.toString() + 'px', 'important');
      ph.style.setProperty(
          'height', aImg.height.toString() + 'px', 'important');

      this.img = img;
      this.angle = 0;
      this.scaleX = 1.0;
      this.scaleY = 1.0;
  },
  /**
   * @param aAngle number from -360 to 360
   * @param aScaleX number from 0.01 to 32.0
   * @param aScaleY number from 0.01 to 32.0
   * @return void
   */
  transformImg: function (aAngle, aScaleX, aScaleY) {
      if (arguments.length < 3) { aScaleY = aScaleX; }

      var img = this.img;
      if (! img) { return; }
      var ph = this.ph;

      var emsg = "The specified value is out of range.\n";
      if (! (aAngle >= -360 && aAngle <= 360)) {
          throw new Error(emsg + "aAngle: " + aAngle);
      }
      var scales = this.SCALES;
      var abscalex = Math.abs(aScaleX);
      var abscaley = Math.abs(aScaleX);
      if (! (abscalex >= scales[0] && abscalex <= scales[scales.length - 1])) {
          throw new Error(emsg + "aScaleX: " + aScaleX);
      }
      if (! (abscaley >= scales[0] && abscaley <= scales[scales.length - 1])) {
          throw new Error(emsg = "aScaleY: " + aScaleY);
      }

      img.style.setProperty(
          '-moz-transform',
          'rotate(' + aAngle + 'deg) ' +
            'scale(' + aScaleX + ', ' + aScaleY + ')',
          'important');
      var t = aAngle * Math.PI / 180;
      var width = img.width;
      var height = img.height;
      var swidth = width * abscalex;
      var sheight = height * abscaley;
      var nwidth = Math.abs(swidth * Math.cos(t) + sheight * Math.sin(t));
      var nheight = Math.abs(swidth * Math.sin(t) + sheight * Math.cos(t));
      img.style.setProperty('margin-top',
                            Math.round((nheight - height) / 2) + 'px',
                            'important');
      if (nwidth < width) {
          img.style.setProperty('margin-left',
                                Math.round((nwidth - width) / 2) + 'px',
                                'important');
      } else {
          img.style.removeProperty('margin-left');
      }
      ph.style.setProperty('width', Math.round(nwidth) + 'px', 'important');
      ph.style.setProperty('height', Math.round(nheight) + 'px', 'important');

      this.angle = aAngle;
      this.scaleX = aScaleX;
      this.scaleY = aScaleY;
  },
  /**
   * @return void
   */
  zoomInImg: function () {
      if (! this.img) { return; }

      var scales = this.SCALES;
      var scalex = this.scaleX;
      var abscalex = Math.abs(scalex);
      var nabscalex = NaN;
      for (var i = 0; i < scales.length; ++i) {
          if (scales[i] > abscalex) { nabscalex = scales[i]; break; }
      }
      if (isNaN(nabscalex)) { return; }
      var nscalex = scalex < 0 ? nabscalex * -1 : nabscalex;
      var nscaley = nscalex * this.scaleY / this.scaleX;
      this.transformImg(this.angle, nscalex, nscaley);
  },
  /**
   * @return void
   */
  zoomOutImg: function () {
      if (! this.img) { return; }

      var scales = this.SCALES;
      var scalex = this.scaleX;
      var abscalex = Math.abs(scalex);
      var nabscalex = NaN;
      for (var i = scales.length - 1; i >= 0; --i) {
          if (scales[i] < abscalex) { nabscalex = scales[i]; break; }
      }
      if (isNaN(nabscalex)) { return; }
      var nscalex = scalex < 0 ? nabscalex * -1 : nabscalex;
      var nscaley = nscalex * this.scaleY / this.scaleX;
      this.transformImg(this.angle, nscalex, nscaley);
  },
  /**
   * @return void
   */
  resetImgZoom: function () {
      if (! this.img) { return; }
      this.transformImg(this.angle, 1.0, 1.0);
  },
  /**
   * @return void
   */
  rotateImgToRight: function () {
      if (! this.img) { return; }

      //round to the nearest 90deg
      var nangle = Math.round(this.angle / 90) * 90 + 90;
      if (nangle >= 360) { nangle -= 360; }
      if (nangle < 0) { nangle += 360; }
      this.transformImg(nangle, this.scaleX, this.scaleY);
  },
  /**
   * @return void
   */
  rotateImgToLeft: function () {
      if (! this.img) { return; }

      //round to the nearest 90deg
      var nangle = Math.round(this.angle / 90) * 90 - 90;
      if (nangle <= -360) { nangle += 360; }
      if (nangle < 0) { nangle += 360; }
      this.transformImg(nangle, this.scaleX, this.scaleY);
  }
};
