/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

(function(){
    var context = _jsaCScript.context;
    if (! context.isTextSelected) { return; }

    var query = context.textSelected;
    var loc = window.location;
    var host = loc.host;
    if (! host) {
        window.alert("Current location doesn't contain a host part.");
        return;
    }
    var site = host + loc.pathname.replace(/\/[^\/]+$/, '');
    var uri = 'https://www.google.com/search?q=site:' +
      encodeURIComponent(site + " " + query) +
      '&ie=utf-8&oe=utf-8';
    _jsaCScript.tabs.create(
        {
          url: uri,
          active: false,
          jsaposition: "afterCurrent"
        });
})();
