/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

(function(){
    var context = _jsaCScript.context;
    if (! context.onImage) { return; }

    var ib = new ImgBox();
    try {
        ib.init();
    } catch (e) {
        window.alert(e.message);
        return;
    }
    var oimg = ib.img;
    var img = context.target;
    if (! oimg || oimg.src != img.src ||
        oimg.width != img.width || oimg.height != img.height) {
        ib.loadImg(img);
    }
    ib.rotateImgToLeft();
    ib.center();
})();
